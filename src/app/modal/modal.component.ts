import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

export interface DialogData {
  name: string;
}

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AppDialog, {
      width: '500px',
      height: '400px',
      data: {name: 'test'}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}

@Component({
  selector: 'app-dialog',
  templateUrl: './app-dialog.html',
})
export class AppDialog {

  constructor(public secondDialog: MatDialog,
              public dialogRef: MatDialogRef<AppDialog>, @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  openSecond(): void {
    const dialogRef = this.secondDialog.open(AppDialog2, {
      width: '250px',
      height: '250px',
      data: {name: 'test'}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog2 was closed');
    });
  }

}

@Component({
  selector: 'app-dialog2',
  templateUrl: './app-dialog2.html',
})
export class AppDialog2 {

  constructor(public dialogRef: MatDialogRef<AppDialog2>, @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
